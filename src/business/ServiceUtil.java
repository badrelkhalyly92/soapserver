package business;

import business.model.Habitat;

import java.util.HashMap;
import java.util.Map;

public class ServiceUtil {
    static Habitat h1 = new Habitat("Touzani","Ahmed",23,90,"Hay Nahda","Rabat");
    static Habitat h2 = new Habitat("Ouazzani","Ali",120,18,"Ouaddahab","Casablanca");
    static Habitat h3 = new Habitat("Bouzidi","Hicham",23,90,"Hay Qods","Oujda");


    static Map<String, Habitat> getRepertoire() {
        Map<String, Habitat> repertoire= new HashMap<String, Habitat>();
        repertoire.put("BK271290", h1);
        repertoire.put("E120987", h2);
        repertoire.put("BH393847", h3);
        return repertoire;
    }
}
