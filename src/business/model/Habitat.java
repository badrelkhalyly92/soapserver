package business.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "habitat")
@XmlAccessorType(XmlAccessType.FIELD)
public class Habitat {
    String nom;
    String prenom;
    int numRue;
    int numMaison;
    String quartier;
    String ville;

    public Habitat(String nom, String prenom, int numRue, int numMaison, String quartier, String ville) {
        this.nom = nom;
        this.prenom = prenom;
        this.numRue = numRue;
        this.numMaison = numMaison;
        this.quartier = quartier;
        this.ville = ville;
    }

    public Habitat() {
    }

    @Override
    public String toString() {
        return "Habitat{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", numRue=" + numRue +
                ", numMaison=" + numMaison +
                ", quartier='" + quartier + '\'' +
                ", ville='" + ville + '\'' +
                '}';
    }
}
