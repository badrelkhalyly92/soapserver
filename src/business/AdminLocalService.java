//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package business;

import business.model.Habitat;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(
        serviceName = "AdminLocalWS"
)
public class AdminLocalService {
    public AdminLocalService() {
    }

    @WebMethod(
            operationName = "retrieveHabitat"
    )
    public Habitat retrieveHabitat(@WebParam(name = "cin") String cin) {
        return (Habitat)ServiceUtil.getRepertoire().get(cin);
    }

    @WebMethod(
            operationName = "getAllHabitat"
    )
    public List<Habitat> getAllHabitat() {
        List listofHb = new ArrayList();
        listofHb.addAll(ServiceUtil.getRepertoire().values());
        return listofHb;
    }


}
